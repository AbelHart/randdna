#include <iostream>
#include <string>
#include <random>

using namespace std;

string randDNA(int seed, string bases, int n){
	
	auto min = 0;
	auto max = bases.size() - 1;
	string DNA = "";
	
	/*mt19937 eng1(rd());
	uniform_int_distribution<int> uniform(min,max);*/
	mt19937 eng1(seed); //seeds the random number engine
	uniform_int_distribution<int> uniform(min, max); //uses a uniform distribution
	
	for(int i = 0; i < n; i++){
		
		int nums = uniform(eng1); 
		DNA = DNA + bases[nums]; //inputs random letters from string bases into string DNA
     }
     return DNA;
 }
